%{!?sources_gpg: %{!?dlrn:%global sources_gpg 0} }
%global sources_gpg_sign 0x5d2d1e4fb8d38e6af76c50d53d4fec30cf5ce3da

%global pname rally_openstack
%{!?upstream_version: %global upstream_version %{version}%{?milestone}}

%global common_desc \
rally-openstack is a collection of plugins for Rally framework \
designed for the OpenStack platform.

Name:             openstack-rally-plugins
Version:          2.1.0
Release:          23%{?dist}
Summary:          A collection of plugins for OpenStack Rally
License:          ASL 2.0
URL:              https://rally.readthedocs.io
Source0:          https://tarballs.openstack.org/rally-openstack/rally-openstack-%{upstream_version}.tar.gz
# Required for tarball sources verification
%if 0%{?sources_gpg} == 1
Source101:        https://tarballs.openstack.org/rally-openstack/rally-openstack-%{upstream_version}.tar.gz.asc
Source102:        https://releases.openstack.org/_static/%{sources_gpg_sign}.txt
%endif
BuildArch:        noarch

Patch0001:        0001-os8806-adding-magnum-config.patch
Patch0002:        0002-os8806-cleanup-resources.patch
Patch0003:        0003-8806-adding-magnum-clusters.patch
Patch0004:        0004-8806-adding-platforms-existing.patch
Patch0005:        0005-8806-adding-scenarios-magnum-k8s_pods.patch
Patch0006:        0006-os8806-adding-scenarios-magnum-swarm_containers.patch
Patch0007:        0007-os8806-adding-scenarios-magnum-utils.patch
Patch0008:        0008-os8806-adding-scenarios-vm-vmtasks.patch
Patch0009:        0009-Imports-fix-to-use-rally_openstack.patch
Patch0010:        0010-Fix-rally_openstack-.cleanup.resources.-CinderVolume.patch
Patch0011:        0011-Remove-allow_ssh-as-it-tries-to-use-security-groups-.patch
Patch0012:        0012-Add-scenario-for-testing-latest-Kubernetes-versions.patch
Patch0013:        0013-Store-tls_disabled-in-the-context-for-ca_certs-clean.patch
Patch0014:        0014-Add-K8sJobs.create_jobs-to-check-job-completion.patch
Patch0015:        0015-Update-kubernetes-client.patch
Patch0016:        0016-Set-Magnum-cluster-create-timeout-to-30-minutes.patch
Patch0017:        0017-Set-Magnum-job-scenario-subtask-name-from-kubernetes.patch
Patch0018:        0018-remove-hardcoded-namespace-for-k8s-job.patch
Patch0019:        0019-Check-failure-status-of-kubernetes-jobs.patch
Patch0020:        0020-Python3-compatibility-issue-after-reading-data-from-.patch
Patch0021:        0021-Use-ping-6-instead-of-ping6.patch
Patch0022:        0022-Another-python2-to-python3-compatibility-issue.patch
Patch0023:        0023-Fix_warning_sshutils.patch
Patch0024:        0024-Fix_warning_yamlutils.patch
Patch0025:        0025-Accept-extra-labels-for-magnum-cluster-creation.patch
Patch0026:        0026-Find-detailed-errors-in-kubernetes-jobs-scenario.patch
Patch0027:        0027-Fix-TypeError-when-creating-swarm-error-message.patch
Patch0028:        0028-Fix-another-swarm-TypeError.patch
Patch0029:        0029-relocate-magnum-scenarios.patch
Patch0030:        0030-Fix-lint-issues.patch
Patch0031:        0031-Add-gitlab-CI-for-testing.patch
Patch0032:        0032-Fix-gitlab-CI.patch
Patch0033:        0033-Remove-K8sJobs-pod-event-and-container-status-checki.patch
Patch0034:        0034-Add-node-flavor-parameter-to-magnum-cluster-context.patch
Patch0035:        0035-Use-retry-interval-for-resource-deletion.patch
Patch0036:        0036-Set-MagnumCluster-cleanup-resource-max-attempts-and-.patch
Patch0037:        0037-k8s-latest-get-hyperkube-tags-from-gitlab-registry.patch
Patch0038:        0038-Add-more-info-to-error-message-when-cluster-is-CREAT.patch
Patch0039:        0039-Fix-k8s-latest-image-tag-prefix.patch
Patch0040:        0040-Include-the-option-to-ping-only-on-a-specific-IP-ver.patch
Patch0041:        0041-Launch-live-migrations-within-the-normal-user-flow.patch
Patch0042:        0042-Remove-some-deprecation-warnings-in-magnum-jobs.patch
Patch0043:        0043-Launch-cold-migrations-within-the-normal-user-flow.patch
Patch0044:        0044-Add-new-scenario-CERNBootServerAttachInterface.patch
Patch0045:        0045-Add-new-scenario-CERNBootServerSetLanDBUserResponsib.patch
Patch0046:        0046-Minor-fixes.patch
Patch0047:        0047-Iterate-over-available-networks.patch
Patch0048:        0048-Add-new-scenario-CERNBootServerLanDBproperties.patch
Patch0049:        0049-Add-new-scenarios-and-util-functions-to-check-landb.patch
Patch0050:        0050-Add-new-scenario-CERNBootServerWithAttachedVolume.patch
Patch0051:        0051-Initial-Octavia-Rally-tests.patch
Patch0052:        0052-Fixes-Octavia-tests.patch
Patch0053:        0053-Minor-fix-in-Octavia-scenarios.patch
Patch0054:        0054-Add-wait-for-loadbalancer-to-be-in-active-status.patch
Patch0055:        0055-Octavia-do-not-run-on-disabled-flavors.patch
Patch0056:        0056-Add-random-value-to-landb-alias-to-avoid-duplicated-.patch

# Required for tarball sources verification
%if 0%{?sources_gpg} == 1
BuildRequires:  /usr/bin/gpgv2
%endif

BuildRequires:    git-core
BuildRequires:    python3-devel
BuildRequires:    python3-pbr
BuildRequires:    python3-setuptools
BuildRequires:    openstack-macros

# test dependencies
BuildRequires:  python3-pytest
BuildRequires:  python3-ddt
BuildRequires:  python3-mock
BuildRequires:  python3-dateutil
BuildRequires:  python3-testtools
BuildRequires:  python3-kubernetes

Requires:       python3-rally
Requires:       python3-boto
Requires:       python3-gnocchiclient
Requires:       python3-keystoneauth1
Requires:       python3-os-faults
Requires:       python3-osprofiler
Requires:       python3-barbicanclient
Requires:       python3-ceilometerclient
Requires:       python3-cinderclient
Requires:       python3-designateclient
Requires:       python3-heatclient
Requires:       python3-glanceclient
Requires:       python3-ironicclient
Requires:       python3-keystoneclient
Requires:       python3-magnumclient
Requires:       python3-manilaclient
Requires:       python3-mistralclient
Requires:       python3-muranoclient
Requires:       python3-monascaclient
Requires:       python3-neutronclient
Requires:       python3-novaclient
Requires:       python3-octaviaclient
Requires:       python3-saharaclient
Requires:       python3-senlinclient
Requires:       python3-swiftclient
Requires:       python3-troveclient
Requires:       python3-zaqarclient
Requires:       python3-requests
Requires:       python3-kubernetes
Requires:       python3-semver

Requires:       python-landbclient
Requires:       python3-dns

%description
%{common_desc}

%prep
# Required for tarball sources verification
%if 0%{?sources_gpg} == 1
%{gpgverify}  --keyring=%{SOURCE102} --signature=%{SOURCE101} --data=%{SOURCE0}
%endif
%autosetup -S git -n rally-openstack-%{upstream_version}

%py_req_cleanup

%build
%{py3_build}

%install
%{py3_install}

%check
# FIXME(chkumar246): watcherclient is not packaged in RDO
# So currently skipping the tests
%{__python3} -m pytest tests/unit || true

%files
%license LICENSE
%{python3_sitelib}/%{pname}
%{python3_sitelib}/%{pname}*.egg-info

%changelog
* Mon Jul 24 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.23
- Add random value to landb-alias to avoid duplicated aliases error

* Mon Jul 10 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.22
- octavia do not run on disabled flavors

* Fri May 12 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.21
- Add missing cci-utils repo

* Fri May 05 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.20
- Rebuild for Alma8 and RHEL8

* Fri Apr 28 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.19
- Add wait for loabalance to be in active status

* Wed Apr 26 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.18
- Minor fix in Octavia scenarios

* Tue Apr 25 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.17
- Fixes Octavia tests

* Fri Apr 21 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.16
- Initial Octavia Rally tests

* Fri Feb 24 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.15
- Add new scenario CERNBootServerWithAttachedVolume

* Tue Feb 07 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.14
- Add python3-dns as requirement

* Thu Feb 02 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.13
- Add missing patches to spec file

* Thu Feb 02 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.12
- Add new scenarios and util functions to check landb-ipv6ready
- Add new scenario CERNBootServerLanDBproperties

* Mon Jan 16 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.11
- Add python-landbclient as requirement

* Fri Jan 13 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.10
- Iterate over available networks to use the same as network_name

* Fri Jan 13 2023 Domingo Rivera Barros <driverab@cern.ch> 2.1.0-1.9
- Minor fixes
- Add new scenario CERNBootServerSetLanDBUserResponsible
- Add new scenario CERNBootServerAttachInterface

* Mon Mar 07 2022 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.0-8
- Add missing dependency on python3-semver

* Wed Jan 26 2022 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.0-7
- Launch cold migrations within the normal user flow

* Fri Jan 14 2022 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.0-6
- Remove some deprecation warnings in magnum jobs

* Fri Jan 14 2022 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.0-5
- Launch live migrations within the normal user flow

* Wed Dec 08 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 2.1.0-4
- Include option to select IP version on ping linux scenarios

* Tue Jun 29 2021 Raul Villar Ramos <raul.villar.ramos@cern.ch> 2.1.0-3
- Apply new patches

* Tue Jun 15 2021 Raul Villar Ramos <raul.villar.ramos@cern.ch> 2.1.0-2
- Release Wallaby with CERN downstream patches

* Fri Apr 02 2021 RDO <dev@lists.rdoproject.org> 2.1.0-1
- Update to 2.1.0

