From e6aa9e0d8e4cb08118092390b8a71888c8077ecb Mon Sep 17 00:00:00 2001
From: Domingo Rivera <domingo.rivera.barros@cern.ch>
Date: Tue, 6 Dec 2022 17:55:58 +0100
Subject: [PATCH 2/3] Add new scenario CERNBootServerSetLanDBUserResponsible

---
 .../task/scenarios/nova/servers.py            | 44 ++++++++++++++++++-
 rally_openstack/task/scenarios/nova/utils.py  | 34 +++++++++++++-
 2 files changed, 75 insertions(+), 3 deletions(-)

diff --git a/rally_openstack/task/scenarios/nova/servers.py b/rally_openstack/task/scenarios/nova/servers.py
index f474f66d..cde4ddb9 100644
--- a/rally_openstack/task/scenarios/nova/servers.py
+++ b/rally_openstack/task/scenarios/nova/servers.py
@@ -1221,8 +1221,8 @@ class BootAndGetConsoleUrl(utils.NovaScenario):
                     platform="openstack")
 class CERNBootServerAndAttachInterfaceAndCheckLanDB(utils.NovaScenario,
     neutron_utils.NeutronScenario):
-    def run(self, image, flavor, network_id=None, min_sleep=0, max_sleep=0,
-            force_delete=False, **kwargs):
+    def run(self, image, flavor, network_id=None, force_delete=False,
+            **kwargs):
         """Create server, then attach the interface to it and check LanDB
 
         This scenario measures the "nova interface-attach" command performance.
@@ -1264,3 +1264,43 @@ class CERNBootServerAndAttachInterfaceAndCheckLanDB(utils.NovaScenario,
         self.neutron.delete_port(new_port)
         self._delete_server(server, force=force_delete)
 
+
+@types.convert(image={"type": "glance_image"},
+               flavor={"type": "nova_flavor"})
+@validation.add("image_valid_on_flavor", flavor_param="flavor",
+                image_param="image")
+@validation.add("required_services", services=[consts.Service.NOVA,
+                                               consts.Service.NEUTRON])
+@validation.add("required_platform", platform="openstack", users=True)
+@scenario.configure(context={"cleanup@openstack": ["nova", "neutron"]},
+                    name=("NovaServers.cern_boot_server_"
+                          "landb_user_responsible"),
+                    platform="openstack")
+class CERNBootServerSetLanDBUserAndResponsible(utils.NovaScenario,
+    neutron_utils.NeutronScenario):
+    def run(self, image, flavor, force_delete=False, **kwargs):
+        """Create server with landb-mainuser and landb-responsible and
+        check it in LanDB
+
+        Args:
+            image (_type_): _description_
+            flavor (_type_): _description_
+            network_id (_type_, optional): _description_. Defaults to None.
+            min_sleep (int, optional): _description_. Defaults to 0.
+            max_sleep (int, optional): _description_. Defaults to 0.
+            force_delete (bool, optional): _description_. Defaults to False.
+        """
+        landb_user = kwargs["meta"]["landb-mainuser"]
+        landb_responsible = kwargs["meta"]["landb-responsible"]
+        server = self._boot_server(image, flavor, **kwargs)
+        landb_info = self._get_landb_device_info(server.name)
+        self._check_lanDB_user_responsible_added(landb_user,
+                                                 landb_responsible,
+                                                 landb_info)
+        self._delete_metadata(server, ['landb-mainuser', 'landb-responsible'])
+        landb_info = self._get_landb_device_info(server.name)
+        self._check_lanDB_user_responsible_deleted(landb_user,
+                                                   landb_responsible,
+                                                   landb_info)
+        self._delete_server(server, force=force_delete)
+
diff --git a/rally_openstack/task/scenarios/nova/utils.py b/rally_openstack/task/scenarios/nova/utils.py
index c99dde5a..7aa29a5d 100644
--- a/rally_openstack/task/scenarios/nova/utils.py
+++ b/rally_openstack/task/scenarios/nova/utils.py
@@ -1295,6 +1295,38 @@ class NovaScenario(neutron_utils.NeutronBaseScenario,
             raise Exception("Failed to detach interface. "
                             "IPs still present in LanDB")
 
+    @atomic.action_timer("nova.check_lanDB_user_responsible_added")
+    def _check_lanDB_user_responsible_added(self, user, responsible, info):
+        if (info.UserPerson.Name == user and
+            info.ResponsiblePerson.Name == responsible):
+            return True
+        else:
+            raise Exception("Failed to set landb properties: "
+                            "landb-mainuser or landb-responsible "
+                            "are not present in landb. Trying to set "
+                            "landb-mainuser: %s and landb-responsible %s "
+                            "Found %s and %s" % (user,
+                                                 responsible,
+                                                 info.UserPerson.Name,
+                                                 info.ResponsiblePerson.Name))
+
+    @atomic.action_timer("nova.check_lanDB_user_responsible_deleted")
+    def _check_lanDB_user_responsible_deleted(self, user, responsible, info):
+        if not (info.UserPerson.Name == user and
+            info.ResponsiblePerson.Name == responsible):
+            return True
+        else:
+            raise Exception("Failed to delete landb properties: "
+                            "landb-mainuser or landb-responsible "
+                            "are still present in landb")
+
+    @atomic.action_timer("nova.delete_metadata")
+    def _delete_metadata(self, server, keys):
+        if not (isinstance(keys, list) and keys):
+            raise exceptions.InvalidArgumentsException(
+                "Param 'keys' should be non-empty 'list'. keys = '%s'" % keys)
+        return self.clients("nova").servers.delete_meta(server.id, keys)
+
     @atomic.action_timer("nova.get_landb_device_info")
     def _get_landb_device_info(self, device_name):
         try:
@@ -1307,4 +1339,4 @@ class NovaScenario(neutron_utils.NeutronBaseScenario,
                 version=CONF.openstack.landb_version)
             return landb.device_info(device_name)
         except Exception as ex:
-            raise("Could not get information from LanDB: %s" % ex)
+            raise Exception("Could not get information from LanDB: %s" % ex)
-- 
2.25.1

