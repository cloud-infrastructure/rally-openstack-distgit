From f6abdafc0837b8990ea9a4fa1e66342386e63801 Mon Sep 17 00:00:00 2001
From: Daniel Failing <daniel.failing@cern.ch>
Date: Wed, 29 Mar 2023 11:09:51 +0200
Subject: [PATCH 1/1] Initial Octavia Rally tests

---
 .../common/services/loadbalancer/octavia.py   |  22 +-
 .../task/scenarios/octavia/full_lb.py         | 261 ++++++++++++++++++
 .../task/scenarios/octavia/l7policy.py        | 108 ++++++++
 .../task/scenarios/octavia/loadbalancers.py   | 194 ++++---------
 .../task/scenarios/octavia/utils.py           |  16 ++
 5 files changed, 459 insertions(+), 142 deletions(-)
 create mode 100644 rally_openstack/task/scenarios/octavia/full_lb.py
 create mode 100644 rally_openstack/task/scenarios/octavia/l7policy.py

diff --git a/rally_openstack/common/services/loadbalancer/octavia.py b/rally_openstack/common/services/loadbalancer/octavia.py
index afa32108..411f7f01 100644
--- a/rally_openstack/common/services/loadbalancer/octavia.py
+++ b/rally_openstack/common/services/loadbalancer/octavia.py
@@ -53,10 +53,11 @@ class Octavia(service.Service):
         return new_lb
 
     @atomic.action_timer("octavia.load_balancer_create")
-    def load_balancer_create(self, subnet_id, description=None,
-                             admin_state=None, project_id=None,
-                             listeners=None, flavor_id=None,
-                             provider=None, vip_qos_policy_id=None):
+    def load_balancer_create(self, network_id, subnet_id=None,
+                             description=None, admin_state=None,
+                             project_id=None, listeners=None,
+                             flavor_id=None, provider=None,
+                             vip_qos_policy_id=None):
         """Create a load balancer
 
         :return:
@@ -69,6 +70,7 @@ class Octavia(service.Service):
             "provider": provider,
             "admin_state_up": admin_state or True,
             "project_id": project_id,
+            "vip_network_id": network_id,
             "vip_subnet_id": subnet_id,
             "vip_qos_policy_id": vip_qos_policy_id,
         }
@@ -632,3 +634,15 @@ class Octavia(service.Service):
             check_interval=(
                 CONF.openstack.octavia_create_loadbalancer_poll_interval)
         )
+
+    @atomic.action_timer("octavia.flavor_list")
+    def flavor_list(self, **kwargs):
+        """List all flavors
+
+        :param kwargs:
+            Parameters to filter on
+        :return:
+            A ``dict`` containing a list of flavors
+        """
+        ret = self._clients.octavia().flavor_list(**kwargs)
+        return ret['flavors']
diff --git a/rally_openstack/task/scenarios/octavia/full_lb.py b/rally_openstack/task/scenarios/octavia/full_lb.py
new file mode 100644
index 00000000..04178999
--- /dev/null
+++ b/rally_openstack/task/scenarios/octavia/full_lb.py
@@ -0,0 +1,261 @@
+# Copyright 2018: Red Hat Inc.
+# All Rights Reserved.
+#
+#    Licensed under the Apache License, Version 2.0 (the "License"); you may
+#    not use this file except in compliance with the License. You may obtain
+#    a copy of the License at
+#
+#         http://www.apache.org/licenses/LICENSE-2.0
+#
+#    Unless required by applicable law or agreed to in writing, software
+#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
+#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
+#    License for the specific language governing permissions and limitations
+#    under the License.
+
+import io
+import select
+import shlex
+import time
+
+from rally.common import logging
+from rally import exceptions
+from rally.task import types
+from rally.task import validation
+from rally.utils import sshutils
+
+from rally_openstack.common import consts
+from rally_openstack.task import scenario
+from rally_openstack.task.scenarios.octavia import utils as octavia_utils
+from rally_openstack.task.scenarios.vm import utils as vm_utils
+
+
+"""Scenarios for Octavia Loadbalancer."""
+
+LOG = logging.getLogger(__name__)
+
+
+class SSHAsync(sshutils.SSH):
+    session = None
+
+    def run_async(self, cmd):
+        """Run a command asynchronously and return the command SSH session
+
+        """
+        if isinstance(cmd, (list, tuple)):
+            cmd = " ".join(shlex.quote(str(p)) for p in cmd)
+
+        stdin = io.StringIO(cmd)
+
+        client = self._get_client()
+        transport = client.get_transport()
+        self.session = transport.open_session()
+        self.session.exec_command("/bin/sh")
+
+        data_to_send = None
+        while True:
+            if self.session.send_ready():
+                if stdin is not None and not stdin.closed:
+                    if not data_to_send:
+                        data_to_send = stdin.read(4096)
+                        if not data_to_send:
+                            stdin.close()
+                            self.session.shutdown_write()
+                            break
+                    sent_bytes = self.session.send(data_to_send)
+                    LOG.debug("sent: %s" % data_to_send[:sent_bytes])
+                    data_to_send = data_to_send[sent_bytes:]
+
+
+class BaseFullLB(octavia_utils.OctaviaBase, vm_utils.VMScenario):
+    _members = []
+
+    def _run_command_async(self, image, flavor, port, command,
+                           username, password, is_backend=True, **kwargs):
+        """Create VM and run asynchronous task.
+
+        """
+        server, fip = self._boot_server_with_fip(image, flavor,
+                                                 use_floating_ip=False,
+                                                 **kwargs)
+
+        try:
+            self._wait_for_ping(fip["ip"])
+            ssh = SSHAsync(username, fip["ip"], port=port,
+                           password=password)
+
+            try:
+                self._wait_for_ssh(ssh)
+                LOG.debug("Running command on %s: %s", fip["ip"], command)
+                ssh.run_async(cmd=command)
+
+                if is_backend:
+                    self._members.append((fip["ip"], ssh))
+                return ssh
+            except exceptions.SSHTimeout:
+                ssh.close()
+                console_logs = self._get_server_console_output(server)
+                LOG.info("VM console logs:\n%s" % console_logs)
+                raise
+        except (exceptions.TimeoutException,
+                exceptions.SSHTimeout):
+            console_logs = self._get_server_console_output(server)
+            LOG.info("VM console logs:\n%s" % console_logs)
+            raise
+
+    def _populate_lb(self, loadbalancers, listener_args, pool_args,
+                     member_args, hm_args):
+        """Populate a loadbalancer with listener, pool and members.
+
+        """
+        for lb in loadbalancers:
+            listener = self.octavia.listener_create(
+                json={
+                    "listener": dict(
+                        loadbalancer_id=lb["id"],
+                        **listener_args)})
+
+            self.octavia.wait_for_loadbalancer_prov_status(lb)
+            pool = self.octavia.pool_create(
+                lb_id=lb["id"],
+                listener_id=listener["listener"]["id"],
+                **pool_args)
+
+            for address, ssh in self._members:
+                self.octavia.wait_for_loadbalancer_prov_status(lb)
+                self.octavia.member_create(
+                    pool_id=pool["id"],
+                    json={"member": dict(
+                        address=address,
+                        protocol_port=member_args["port"])})
+
+            self.octavia.wait_for_loadbalancer_prov_status(lb)
+            self.octavia.health_monitor_create(
+                json={"healthmonitor": dict(
+                    pool_id=pool["id"],
+                    **hm_args)})
+
+    def _prepare_backend(self, port, **kwargs):
+        """Test the connectivity of the LBs
+
+        """
+        self._run_command_async(port=22, is_backend=True, **kwargs)
+
+    def _test_lb(self, loadbalancers, test_command, timeout=60, **kwargs):
+        """Test the connectivity of the LBs
+
+        """
+        create_args = kwargs.copy()
+        try:
+            create_args.pop("command")
+            create_args.pop("port")
+        except KeyError:
+            pass
+
+        for lb in loadbalancers:
+            cmd = []
+            if isinstance(test_command, list):
+                for c in test_command:
+                    cmd.append(c.format(lb["vip_address"]))
+            else:
+                cmd.append(test_command.format(lb["vip_address"]))
+            ssh = self._run_command_async(port=22,
+                                          command=cmd,
+                                          is_backend=False,
+                                          **create_args)
+            start_time = time.time()
+            LOG.debug("session: %s", ssh.session)
+            while True:
+                r, w, e = select.select([ssh.session], [], [ssh.session], 1)
+                if ssh.session.recv_ready():
+                    data = ssh.session.recv(4096)
+                    LOG.debug("stdout: %r" % data)
+                    continue
+                if ssh.session.recv_stderr_ready():
+                    stderr_data = ssh.session.recv_stderr(4096)
+                    LOG.debug("stderr: %r" % stderr_data)
+                    continue
+                if ssh.session.exit_status_ready():
+                    LOG.debug("exit status ready")
+                    break
+
+                if timeout and (time.time() - timeout) > start_time:
+                    args = {"cmd": cmd}
+                    raise exceptions.SSHTimeout("Timeout executing "
+                                                "test_command: %s"
+                                                % args["cmd"])
+                if e:
+                    LOG.info(e)
+
+            code = 255
+            if ssh.session is not None:
+                code = ssh.session.recv_exit_status()
+
+            ssh.close()
+            if code != 0:
+                raise Exception("test_command exited with %d instead of "
+                                "exit code 0!" % code)
+
+    def _cleanup(self):
+        for address, ssh in self._members:
+            try:
+                ssh.close()
+            except AttributeError:
+                pass
+
+
+@types.convert(image={"type": "glance_image"},
+               flavor={"type": "nova_flavor"})
+@validation.add("image_valid_on_flavor", flavor_param="flavor",
+                image_param="image", fail_on_404_image=False)
+@validation.add("required_services", services=[consts.Service.NOVA,
+                                               consts.Service.CINDER,
+                                               consts.Service.OCTAVIA])
+@validation.add("required_platform", platform="openstack", users=True)
+@scenario.configure(context={"cleanup@openstack": ["nova", "octavia"],
+                             "keypair@openstack": {},
+                             "allow_ssh@openstack": None},
+                    name="Octavia.cern_create_failover_delete_full_lb",
+                    platform="openstack")
+class CreateFailoverFullLB(BaseFullLB):
+
+    def run(self, network_id, flavor, image,
+            member_args, listener_args, pool_args, hm_args, test_command,
+            admin_state=True, **kwargs):
+        """Create a loadbalancer per each subnet and then list loadbalancers.
+
+        :param admin_state: The administrative state of the loadbalancer,
+            which is up(true) or down(false)
+        :param kwargs: All other arguments for the loadbalancers
+        """
+        try:
+            loadbalancers = self._create_load_balancer_every_flavor(
+                network_id=network_id, admin_state=admin_state, **kwargs)
+            self._prepare_backend(flavor=flavor, image=image, **member_args)
+            self._populate_lb(loadbalancers, listener_args, pool_args,
+                              member_args, hm_args)
+            for loadbalancer in loadbalancers:
+                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
+
+            self._test_lb(loadbalancers, test_command,
+                          flavor=flavor, image=image, **member_args)
+
+            for loadbalancer in loadbalancers:
+                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
+                self.octavia.load_balancer_failover(
+                    loadbalancer["id"])
+
+            for loadbalancer in loadbalancers:
+                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
+
+            self._test_lb(loadbalancers, test_command,
+                          flavor=flavor, image=image, **member_args)
+
+            for loadbalancer in loadbalancers:
+                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
+                self.octavia.load_balancer_delete(
+                    loadbalancer["id"], cascade=True)
+        except Exception:
+            raise
+        finally:
+            self._cleanup()
diff --git a/rally_openstack/task/scenarios/octavia/l7policy.py b/rally_openstack/task/scenarios/octavia/l7policy.py
new file mode 100644
index 00000000..0750ad2c
--- /dev/null
+++ b/rally_openstack/task/scenarios/octavia/l7policy.py
@@ -0,0 +1,108 @@
+# Copyright 2018: Red Hat Inc.
+# All Rights Reserved.
+#
+#    Licensed under the Apache License, Version 2.0 (the "License"); you may
+#    not use this file except in compliance with the License. You may obtain
+#    a copy of the License at
+#
+#         http://www.apache.org/licenses/LICENSE-2.0
+#
+#    Unless required by applicable law or agreed to in writing, software
+#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
+#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
+#    License for the specific language governing permissions and limitations
+#    under the License.
+
+from rally.common import logging
+from rally.task import types
+from rally.task import validation
+
+from rally_openstack.common import consts
+from rally_openstack.task import scenario
+from rally_openstack.task.scenarios.octavia import full_lb as full_lb
+
+
+"""Scenarios for Octavia Loadbalancer."""
+
+LOG = logging.getLogger(__name__)
+
+
+@types.convert(image={"type": "glance_image"},
+               flavor={"type": "nova_flavor"})
+@validation.add("image_valid_on_flavor", flavor_param="flavor",
+                image_param="image", fail_on_404_image=False)
+@validation.add("required_services", services=[consts.Service.NOVA,
+                                               consts.Service.CINDER,
+                                               consts.Service.OCTAVIA])
+@validation.add("required_platform", platform="openstack", users=True)
+@scenario.configure(context={"cleanup@openstack": ["nova", "octavia"],
+                             "keypair@openstack": {},
+                             "allow_ssh@openstack": None},
+                    name="Octavia.cern_create_delete_l7_lb",
+                    platform="openstack")
+class CreateFailoverL7LB(full_lb.BaseFullLB):
+
+    def _populate_lb_l7(self, loadbalancers, listener_args,
+                        l7policy_args, l7rule_args):
+        """Populate a loadbalancer with listener, pool and members.
+
+        """
+        for lb in loadbalancers:
+            listener = self.octavia.listener_create(json={
+                "listener": dict(
+                    loadbalancer_id=lb["id"],
+                    **listener_args
+                )})
+
+            self.octavia.wait_for_loadbalancer_prov_status(lb)
+            l7policy = self.octavia.l7policy_create(json={
+                "l7policy": dict(
+                    listener_id=listener["listener"]["id"],
+                    **l7policy_args
+                )})
+            self.octavia.wait_for_loadbalancer_prov_status(lb)
+            self.octavia.l7rule_create(json={
+                "l7rule": dict(
+                    l7policy_id=l7policy["l7policy"]["id"],
+                    **l7rule_args
+                )})
+
+    def run(self, network_id, flavor, image,
+            listener_args, l7policy_args, l7rule_args, test_command,
+            member_args, admin_state=True, **kwargs):
+        """Create a loadbalancer per each subnet and test L7 policies on it.
+
+        :param admin_state: The administrative state of the loadbalancer,
+            which is up(true) or down(false)
+        :param kwargs: All other arguments for the loadbalancers
+        """
+        try:
+            loadbalancers = self._create_load_balancer_every_flavor(
+                network_id=network_id, admin_state=admin_state, **kwargs)
+            self._prepare_backend(flavor=flavor, image=image, **member_args)
+            self._populate_lb_l7(loadbalancers, listener_args,
+                                 l7policy_args, l7rule_args)
+            for loadbalancer in loadbalancers:
+                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
+
+            self._test_lb(loadbalancers, test_command,
+                          flavor=flavor, image=image, **member_args)
+
+            for loadbalancer in loadbalancers:
+                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
+                self.octavia.load_balancer_failover(loadbalancer["id"])
+
+            for loadbalancer in loadbalancers:
+                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
+
+            self._test_lb(loadbalancers, test_command,
+                          flavor=flavor, image=image, **member_args)
+
+            for loadbalancer in loadbalancers:
+                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
+                self.octavia.load_balancer_delete(
+                    loadbalancer["id"], cascade=True)
+        except Exception:
+            raise
+        finally:
+            self._cleanup()
diff --git a/rally_openstack/task/scenarios/octavia/loadbalancers.py b/rally_openstack/task/scenarios/octavia/loadbalancers.py
index c80a4788..c643c367 100644
--- a/rally_openstack/task/scenarios/octavia/loadbalancers.py
+++ b/rally_openstack/task/scenarios/octavia/loadbalancers.py
@@ -13,53 +13,33 @@
 #    License for the specific language governing permissions and limitations
 #    under the License.
 
+from rally.common import logging
 from rally.task import validation
-
 from rally_openstack.common import consts
 from rally_openstack.task import scenario
 from rally_openstack.task.scenarios.octavia import utils as octavia_utils
 
+LOG = logging.getLogger(__name__)
+
 """Scenarios for Octavia Loadbalancer."""
 
 
 @validation.add("required_services", services=[consts.Service.OCTAVIA])
 @validation.add("required_platform", platform="openstack", users=True)
-@validation.add("required_contexts", contexts=["network"])
 @scenario.configure(context={"cleanup@openstack": ["octavia"]},
                     name="Octavia.create_and_list_loadbalancers",
                     platform="openstack")
 class CreateAndListLoadbalancers(octavia_utils.OctaviaBase):
 
-    def run(self, description=None, admin_state=True,
-            listeners=None, flavor_id=None, provider=None,
-            vip_qos_policy_id=None):
+    def run(self, admin_state=True, **kwargs):
         """Create a loadbalancer per each subnet and then list loadbalancers.
 
-        :param description: Human-readable description of the loadbalancer
         :param admin_state: The administrative state of the loadbalancer,
             which is up(true) or down(false)
-        :param listeners: The associated listener id, if any
-        :param flavor_id: The ID of the flavor
-        :param provider: Provider name for the loadbalancer
-        :param vip_qos_policy_id: The ID of the QoS policy
+        :param kwargs: All other arguments for the loadbalancers
         """
-        subnets = []
-        loadbalancers = []
-        networks = self.context.get("tenant", {}).get("networks", [])
-        project_id = self.context["tenant"]["id"]
-        for network in networks:
-            subnets.extend(network.get("subnets", []))
-        for subnet_id in subnets:
-            lb = self.octavia.load_balancer_create(
-                subnet_id=subnet_id,
-                description=description,
-                admin_state=admin_state,
-                project_id=project_id,
-                listeners=listeners,
-                flavor_id=flavor_id,
-                provider=provider,
-                vip_qos_policy_id=vip_qos_policy_id)
-            loadbalancers.append(lb)
+        loadbalancers = self._create_load_balancer_every_flavor(
+            admin_state=admin_state, **kwargs)
 
         for loadbalancer in loadbalancers:
             self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
@@ -68,42 +48,20 @@ class CreateAndListLoadbalancers(octavia_utils.OctaviaBase):
 
 @validation.add("required_services", services=[consts.Service.OCTAVIA])
 @validation.add("required_platform", platform="openstack", users=True)
-@validation.add("required_contexts", contexts=["network"])
 @scenario.configure(context={"cleanup@openstack": ["octavia"]},
                     name="Octavia.create_and_delete_loadbalancers",
                     platform="openstack")
 class CreateAndDeleteLoadbalancers(octavia_utils.OctaviaBase):
 
-    def run(self, description=None, admin_state=True,
-            listeners=None, flavor_id=None, provider=None,
-            vip_qos_policy_id=None):
-        """Create a loadbalancer per each subnet and then delete loadbalancer
+    def run(self, admin_state=True, **kwargs):
+        """Create a loadbalancer per each subnet and then list loadbalancers.
 
-        :param description: Human-readable description of the loadbalancer
         :param admin_state: The administrative state of the loadbalancer,
             which is up(true) or down(false)
-        :param listeners: The associated listener id, if any
-        :param flavor_id: The ID of the flavor
-        :param provider: Provider name for the loadbalancer
-        :param vip_qos_policy_id: The ID of the QoS policy
+        :param kwargs: All other arguments for the loadbalancers
         """
-        subnets = []
-        loadbalancers = []
-        networks = self.context.get("tenant", {}).get("networks", [])
-        project_id = self.context["tenant"]["id"]
-        for network in networks:
-            subnets.extend(network.get("subnets", []))
-        for subnet_id in subnets:
-            lb = self.octavia.load_balancer_create(
-                subnet_id=subnet_id,
-                description=description,
-                admin_state=admin_state,
-                project_id=project_id,
-                listeners=listeners,
-                flavor_id=flavor_id,
-                provider=provider,
-                vip_qos_policy_id=vip_qos_policy_id)
-            loadbalancers.append(lb)
+        loadbalancers = self._create_load_balancer_every_flavor(
+            admin_state=admin_state, **kwargs)
 
         for loadbalancer in loadbalancers:
             self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
@@ -113,48 +71,25 @@ class CreateAndDeleteLoadbalancers(octavia_utils.OctaviaBase):
 
 @validation.add("required_services", services=[consts.Service.OCTAVIA])
 @validation.add("required_platform", platform="openstack", users=True)
-@validation.add("required_contexts", contexts=["network"])
 @scenario.configure(context={"cleanup@openstack": ["octavia"]},
                     name="Octavia.create_and_update_loadbalancers",
                     platform="openstack")
 class CreateAndUpdateLoadBalancers(octavia_utils.OctaviaBase):
 
-    def run(self, description=None, admin_state=True,
-            listeners=None, flavor_id=None, provider=None,
-            vip_qos_policy_id=None):
-        """Create a loadbalancer per each subnet and then update
+    def run(self, admin_state=True, **kwargs):
+        """Create a loadbalancer per each subnet and then list loadbalancers.
 
-        :param description: Human-readable description of the loadbalancer
         :param admin_state: The administrative state of the loadbalancer,
             which is up(true) or down(false)
-        :param listeners: The associated listener id, if any
-        :param flavor_id: The ID of the flavor
-        :param provider: Provider name for the loadbalancer
-        :param vip_qos_policy_id: The ID of the QoS policy
+        :param kwargs: All other arguments for the loadbalancers
         """
-        subnets = []
-        loadbalancers = []
-        networks = self.context.get("tenant", {}).get("networks", [])
-        project_id = self.context["tenant"]["id"]
-        for network in networks:
-            subnets.extend(network.get("subnets", []))
-        for subnet_id in subnets:
-            lb = self.octavia.load_balancer_create(
-                subnet_id=subnet_id,
-                description=description,
-                admin_state=admin_state,
-                project_id=project_id,
-                listeners=listeners,
-                flavor_id=flavor_id,
-                provider=provider,
-                vip_qos_policy_id=vip_qos_policy_id)
-            loadbalancers.append(lb)
+        loadbalancers = self._create_load_balancer_every_flavor(
+            admin_state=admin_state, **kwargs)
 
+        for loadbalancer in loadbalancers:
             update_loadbalancer = {
                 "name": self.generate_random_name()
             }
-
-        for loadbalancer in loadbalancers:
             self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
             self.octavia.load_balancer_set(
                 lb_id=loadbalancer["id"],
@@ -163,43 +98,20 @@ class CreateAndUpdateLoadBalancers(octavia_utils.OctaviaBase):
 
 @validation.add("required_services", services=[consts.Service.OCTAVIA])
 @validation.add("required_platform", platform="openstack", users=True)
-@validation.add("required_contexts", contexts=["network"])
 @scenario.configure(context={"cleanup@openstack": ["octavia"]},
                     name="Octavia.create_and_stats_loadbalancers",
                     platform="openstack")
 class CreateAndShowStatsLoadBalancers(octavia_utils.OctaviaBase):
 
-    def run(self, description=None, admin_state=True,
-            listeners=None, flavor_id=None, provider=None,
-            vip_qos_policy_id=None):
-        """Create a loadbalancer per each subnet and stats
+    def run(self, admin_state=True, **kwargs):
+        """Create a loadbalancer per each subnet and then list loadbalancers.
 
-        :param description: Human-readable description of the loadbalancer
         :param admin_state: The administrative state of the loadbalancer,
             which is up(true) or down(false)
-        :param listeners: The associated listener id, if any
-        :param flavor_id: The ID of the flavor
-        :param provider: Provider name for the loadbalancer
-        :param vip_qos_policy_id: The ID of the QoS policy
+        :param kwargs: All other arguments for the loadbalancers
         """
-        subnets = []
-        loadbalancers = []
-        networks = self.context.get("tenant", {}).get("networks", [])
-        project_id = self.context["tenant"]["id"]
-        for network in networks:
-            subnets.extend(network.get("subnets", []))
-        for subnet_id in subnets:
-            lb = self.octavia.load_balancer_create(
-                subnet_id=subnet_id,
-                description=description,
-                admin_state=admin_state,
-                project_id=project_id,
-                listeners=listeners,
-                flavor_id=flavor_id,
-                provider=provider,
-                vip_qos_policy_id=vip_qos_policy_id)
-            loadbalancers.append(lb)
-
+        loadbalancers = self._create_load_balancer_every_flavor(
+            admin_state=admin_state, **kwargs)
         for loadbalancer in loadbalancers:
             self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
             self.octavia.load_balancer_stats_show(
@@ -208,42 +120,48 @@ class CreateAndShowStatsLoadBalancers(octavia_utils.OctaviaBase):
 
 @validation.add("required_services", services=[consts.Service.OCTAVIA])
 @validation.add("required_platform", platform="openstack", users=True)
-@validation.add("required_contexts", contexts=["network"])
 @scenario.configure(context={"cleanup@openstack": ["octavia"]},
                     name="Octavia.create_and_show_loadbalancers",
                     platform="openstack")
 class CreateAndShowLoadBalancers(octavia_utils.OctaviaBase):
 
-    def run(self, description=None, admin_state=True,
-            listeners=None, flavor_id=None, provider=None,
-            vip_qos_policy_id=None):
-        """Create a loadbalancer per each subnet and then compare
+    def run(self, admin_state=True, **kwargs):
+        """Create a loadbalancer per each subnet and then list loadbalancers.
 
-        :param description: Human-readable description of the loadbalancer
         :param admin_state: The administrative state of the loadbalancer,
             which is up(true) or down(false)
-        :param listeners: The associated listener id, if any
-        :param flavor_id: The ID of the flavor
-        :param provider: Provider name for the loadbalancer
-        :param vip_qos_policy_id: The ID of the QoS policy
+        :param kwargs: All other arguments for the loadbalancers
         """
-        subnets = []
-        loadbalancers = []
-        networks = self.context.get("tenant", {}).get("networks", [])
-        project_id = self.context["tenant"]["id"]
-        for network in networks:
-            subnets.extend(network.get("subnets", []))
-        for subnet_id in subnets:
-            lb = self.octavia.load_balancer_create(
-                subnet_id=subnet_id,
-                description=description,
-                admin_state=admin_state,
-                project_id=project_id,
-                listeners=listeners,
-                flavor_id=flavor_id,
-                provider=provider,
-                vip_qos_policy_id=vip_qos_policy_id)
-            loadbalancers.append(lb)
+        loadbalancers = self._create_load_balancer_every_flavor(
+            admin_state=admin_state, **kwargs)
+
+        for loadbalancer in loadbalancers:
+            self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
+            self.octavia.load_balancer_show(
+                loadbalancer["id"])
+
+
+@validation.add("required_services", services=[consts.Service.OCTAVIA])
+@validation.add("required_platform", platform="openstack", users=True)
+@scenario.configure(context={"cleanup@openstack": ["octavia"]},
+                    name="Octavia.create_and_failover_loadbalancers",
+                    platform="openstack")
+class CreateAndFailoverLoadBalancers(octavia_utils.OctaviaBase):
+
+    def run(self, admin_state=True, **kwargs):
+        """Create a loadbalancer per each subnet and then list loadbalancers.
+
+        :param admin_state: The administrative state of the loadbalancer,
+            which is up(true) or down(false)
+        :param kwargs: All other arguments for the loadbalancers
+        """
+        loadbalancers = self._create_load_balancer_every_flavor(
+            admin_state=admin_state, **kwargs)
+
+        for loadbalancer in loadbalancers:
+            self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
+            self.octavia.load_balancer_failover(
+                loadbalancer["id"])
 
         for loadbalancer in loadbalancers:
             self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
diff --git a/rally_openstack/task/scenarios/octavia/utils.py b/rally_openstack/task/scenarios/octavia/utils.py
index 7e6e6e16..8e47428f 100644
--- a/rally_openstack/task/scenarios/octavia/utils.py
+++ b/rally_openstack/task/scenarios/octavia/utils.py
@@ -13,9 +13,13 @@
 #    License for the specific language governing permissions and limitations
 #    under the License.
 
+from rally.common import logging
+
 from rally_openstack.common.services.loadbalancer import octavia
 from rally_openstack.task import scenario
 
+LOG = logging.getLogger(__name__)
+
 
 class OctaviaBase(scenario.OpenStackScenario):
     """Base class for Octavia scenarios with basic atomic actions."""
@@ -30,3 +34,15 @@ class OctaviaBase(scenario.OpenStackScenario):
             self.octavia = octavia.Octavia(
                 self._clients, name_generator=self.generate_random_name,
                 atomic_inst=self.atomic_actions())
+
+    def _create_load_balancer_every_flavor(self, network_id, **kwargs):
+        loadbalancers = []
+        flavor_list = [None]
+        flavor_list += [f["id"] for f in self.octavia.flavor_list()]
+        for flavor_id in set(flavor_list):
+            lb = self.octavia.load_balancer_create(
+                network_id=network_id,
+                flavor_id=flavor_id,
+                **kwargs)
+            loadbalancers.append(lb)
+        return loadbalancers
-- 
2.25.1

