From ba6b090c6585520a71562cd91f242dfdcd498d7f Mon Sep 17 00:00:00 2001
From: Jose Castro Leon <jose.castro.leon@cern.ch>
Date: Wed, 26 Jan 2022 09:57:57 +0100
Subject: [PATCH 43/43] Launch cold migrations within the normal user flow

Signed-off-by: Jose Castro Leon <jose.castro.leon@cern.ch>
---
 rally_openstack/task/scenarios/nova/servers.py |  7 ++++---
 rally_openstack/task/scenarios/nova/utils.py   | 13 +++++++------
 tests/unit/task/scenarios/nova/test_servers.py |  2 +-
 3 files changed, 12 insertions(+), 10 deletions(-)

diff --git a/rally_openstack/task/scenarios/nova/servers.py b/rally_openstack/task/scenarios/nova/servers.py
index 17b28883..dc019c3a 100644
--- a/rally_openstack/task/scenarios/nova/servers.py
+++ b/rally_openstack/task/scenarios/nova/servers.py
@@ -847,13 +847,14 @@ class BootServerAttachCreatedVolumeAndLiveMigrate(utils.NovaScenario,
                 image_param="image")
 @validation.add("required_services", services=[consts.Service.NOVA])
 @validation.add("required_platform", platform="openstack",
-                admin=True, users=True)
+                users=True)
 @scenario.configure(context={"cleanup@openstack": ["nova"]},
                     name="NovaServers.boot_and_migrate_server",
                     platform="openstack")
 class BootAndMigrateServer(utils.NovaScenario):
 
-    def run(self, image, flavor, **kwargs):
+    def run(self, image, flavor, admin=True,
+            skip_compute_nodes_check=False, **kwargs):
         """Migrate a server.
 
         This scenario launches a VM on a compute node available in
@@ -865,7 +866,7 @@ class BootAndMigrateServer(utils.NovaScenario):
         :param kwargs: Optional additional arguments for server creation
         """
         server = self._boot_server(image, flavor, **kwargs)
-        self._migrate(server)
+        self._migrate(server, admin, skip_compute_nodes_check)
         # NOTE(wtakase): This is required because cold migration and resize
         #                share same code path.
         confirm = kwargs.get("confirm", True)
diff --git a/rally_openstack/task/scenarios/nova/utils.py b/rally_openstack/task/scenarios/nova/utils.py
index 56d3698e..fcc60599 100644
--- a/rally_openstack/task/scenarios/nova/utils.py
+++ b/rally_openstack/task/scenarios/nova/utils.py
@@ -820,7 +820,7 @@ class NovaScenario(neutron_utils.NeutronBaseScenario,
                     "but instance did not change host: %s" % host_pre_migrate)
 
     @atomic.action_timer("nova.migrate")
-    def _migrate(self, server, skip_compute_nodes_check=False,
+    def _migrate(self, server, admin=True, skip_compute_nodes_check=False,
                  skip_host_check=False):
         """Run migration of the given server.
 
@@ -836,9 +836,10 @@ class NovaScenario(neutron_utils.NeutronBaseScenario,
                 raise exceptions.RallyException("Less than 2 compute nodes,"
                                                 " skipping Migration")
 
-        server_admin = self.admin_clients("nova").servers.get(server.id)
-        host_pre_migrate = getattr(server_admin, "OS-EXT-SRV-ATTR:host")
-        server_admin.migrate()
+        client = self.admin_clients("nova") if admin else self.clients("nova")
+        server = client.servers.get(server.id)
+        host_pre_migrate = getattr(server, "OS-EXT-SRV-ATTR:host")
+        server.migrate()
         utils.wait_for_status(
             server,
             ready_statuses=["VERIFY_RESIZE"],
@@ -848,8 +849,8 @@ class NovaScenario(neutron_utils.NeutronBaseScenario,
                 CONF.openstack.nova_server_migrate_poll_interval)
         )
         if not skip_host_check:
-            server_admin = self.admin_clients("nova").servers.get(server.id)
-            host_after_migrate = getattr(server_admin, "OS-EXT-SRV-ATTR:host")
+            server = client.servers.get(server.id)
+            host_after_migrate = getattr(server, "OS-EXT-SRV-ATTR:host")
             if host_pre_migrate == host_after_migrate:
                 raise exceptions.RallyException(
                     "Migration failed: Migration complete but instance"
diff --git a/tests/unit/task/scenarios/nova/test_servers.py b/tests/unit/task/scenarios/nova/test_servers.py
index 29de61df..4fe21821 100644
--- a/tests/unit/task/scenarios/nova/test_servers.py
+++ b/tests/unit/task/scenarios/nova/test_servers.py
@@ -821,7 +821,7 @@ class NovaServersTestCase(test.ScenarioTestCase):
                                                       fakearg="fakearg",
                                                       confirm=confirm)
 
-        scenario._migrate.assert_called_once_with(fake_server)
+        scenario._migrate.assert_called_once_with(fake_server, True, False)
 
         if confirm:
             scenario._resize_confirm.assert_called_once_with(fake_server,
-- 
2.34.1

